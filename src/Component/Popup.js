import React from "react";

export default function Popup(props) {
  console.log('props :>> ', props);
  if(props.data===''){
    return
  }
  let boundry;
  let currencies;
  let native = Object.values(Object.values(props.data.name.nativeName)[0])[0]
  if(props.data.currencies==undefined){
    currencies = "NaN"
  }else{
    currencies = Object.keys(props.data.currencies)[0]
  }let population = props.data.subregion
  let lang = Object.values(props.data.languages).toString()  
  let subRegion = props.data.subregion;
  let capital;
  if(props.data.capital==undefined){
    capital='NaN'
  }else{
    capital = props.data.capital.toString();
  }
  if(props.data.borders==undefined){
    boundry='No Border Countries'
  }else{
    boundry = props.data.borders.toString();
  }
  console.log(boundry);

  // console.log(props.data.name.official);

  // console.log('props :>> ', props.data.name);
  return (
    <div className="popup">
      <img src={props.data.flags.svg} />
      <div>
        <h1>{props.data.name.common}</h1>
     
      <div className="details">
        <p>
          <strong>Native Name : </strong>{native}
        </p>
        <p>
          <strong>Top Level Domain :</strong>be
        </p>
        <p>
          <strong>Currency : </strong>{currencies}
        </p>
        <p>
          <strong>Population : </strong>{currencies}
        </p>
        <p>
          <strong>Region : </strong>{population}
        </p>
        <p>
          <strong>Language :</strong>{lang}
        </p>
        <p>
          <strong>Sub Region : </strong>{subRegion}
        </p>
        <p>
          <strong>Capital : </strong>{capital}
        </p>
      </div>
      <div className="closeButton">
        <strong>Border</strong>
        <div className="borderCountries">{boundry}</div>
      </div>
      <button className="btn" onClick={()=>{props.disappear()}}>clear</button>
    </div>
    </div>
  );
}
