import React, { Component } from 'react'

export default class CountriesComponent extends Component {
  constructor(props){
    super(props)


  }


  render() {
    return (
      <div className="country" onClick={(e)=>{
        this.props.findSelectedCountry(this.props.countryName);
      }}>
        <img src={this.props.imgLink} className="country-img-top" alt="..."/>
        <h3 className="country-text"><strong>Country:-  </strong>{this.props.countryName}</h3>
        <p className="country-text"><strong>Population:-  </strong>{this.props.countryPopulation}</p>
        <p className="country-text"><strong>Region:-  </strong>{this.props.countryRegion}</p>
        <p className='country-text'><strong>Capital:-  </strong>{this.props.countryCapital}</p>
      </div>
    )
  }
}
