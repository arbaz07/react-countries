import React, { Component } from "react";
import CountryCard from "./Component/CountryCard";
import CountryDetails from "./Component/CountryDetails";
import Header from "./Component/Header";
import Popup from "./Component/Popup";
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      popup:'',
      DataisLoaded: false,
      countryToDisplay: [],
      selectedCountry: '',
    };
  }

  findSelectedCountry = (name) => {

    let countryObject = {};
    this.state.items.forEach((item)=>{
      if(item.name.common===name){
        countryObject=item;
      }
    })
    this.setState((previousState)=>{
      return{
        selectedCountry: countryObject,
        popup: <Popup data = {this.state.selectedCountry} disappear={this.popupDisappear}/>
      }
    })
  
  }
  popupDisappear=()=>{
    console.log(' popup disappear:>> ' );
    this.setState({
      popup:''
    })
  }

  componentDidMount = () => {
    fetch("https://restcountries.com/v3.1/all")
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json,
          DataisLoaded: true,
          countryToDisplay: json,
          // selectedCountry:json,
        });
      });
  };

  filerByCountry = (name) => {
    name = name.toUpperCase();
    this.setState((state) => {
      return {
        countryToDisplay: state.items.filter((country) => {
          let searchName = country.name.common.toUpperCase();
          return searchName.includes(name);
        }),
      };
    });
  };

  filerByRegion = (name) => {
    name = name.toUpperCase();
    // console.log("name :>> ", name);
    this.setState((previousState) => {
      if (name == "ALL") {
        return {
          countryToDisplay: this.state.items,
        };
      } else {
        return {
          countryToDisplay: previousState.items.filter((country) => {
            let searchName = country.region.toUpperCase();
            return searchName.includes(name);
          }),
        };
      }
    });
  };

  render() {
    return (
      <div className="container">
      
        <Header
          className="header"
          search={this.filerByCountry}
          searchByRegion={this.filerByRegion}
        />


        <div className='popupDiv'>{this.state.popup}</div>



        <div className="all-countries">
          {this.state.countryToDisplay.map((country, index) => {
            return (
              <CountryCard
                key={index}
                imgLink={country.flags.svg}
                countryName={country.name.common}
                countryPopulation={country.population}
                countryRegion={country.region}
                countryCapital={country.capital}
                findSelectedCountry={this.findSelectedCountry}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default App;
